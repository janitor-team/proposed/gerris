Source: gerris
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders:
 Drew Parsons <dparsons@debian.org>
Section: science
Priority: optional
Build-Depends:
 debhelper-compat (= 12),
 dh-exec,
 gfortran,
 libglib2.0-dev,
 libgsl0-dev,
 libgts-bin (>= 0.7.6+darcs121130-1.2),
 libgts-dev (>= 0.7.6+darcs121130-1.2),
 libnetcdf-dev,
 mpi-default-dev,
 python3,
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/science-team/gerris/
Vcs-Git: https://salsa.debian.org/science-team/gerris.git
Homepage: http://gfs.sourceforge.net/
Rules-Requires-Root: no

Package: gerris
Architecture: any
Depends:
 gcc,
 ${misc:Depends},
 ${shlibs:Depends}
Recommends:
 gfsview
Suggests:
 python3
Breaks:
 gerris-mpi (<< 20131206+dfsg-7)
Replaces:
 gerris-mpi (<< 20131206+dfsg-7)
Description: Fluid Flow Solver
 Gerris is a system for the solution of the partial differential
 equations describing fluid flow.
 .
 A brief summary of its main (current) features:
 .
   * Solves the time-dependent incompressible variable-density Euler,
     Stokes or Navier-Stokes equations
   * Adaptive mesh refinement: the resolution is adapted dynamically to
     the features of the flow
   * Entirely automatic mesh generation in complex geometries
   * Second-order in space and time
   * Unlimited number of advected/diffused passive tracers
   * Flexible specification of additional source terms
   * Portable parallel support using the MPI library
   * Volume of Fluid advection scheme for interfacial flows
 .
 This package has MPI support built in.

Package: libgfs-1.3-2
Architecture: any
Section: devel
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Breaks:
 libgfs-mpi-1.3-2 (<< 20131206+dfsg-7)
Replaces:
 libgfs-mpi-1.3-2 (<< 20131206+dfsg-7)
Description: Fluid Flow Solver -- shared libraries
 Gerris is a system for the solution of the partial differential
 equations describing fluid flow.
 .
 This package contains the shared libraries for libgfs.
 .
 This package has MPI support built in.

Package: libgfs-dev
Architecture: any
Section: libdevel
Depends:
 ffmpeg,
 libgfs-1.3-2 (= ${binary:Version}),
 libgts-dev,
 ${misc:Depends},
 ${shlibs:Depends}
Breaks:
 libgfs-mpi-dev (<< 20131206+dfsg-7)
Replaces:
 libgfs-mpi-dev (<< 20131206+dfsg-7)
Description: Fluid Flow Solver -- development libraries and headers
 Gerris is a system for the solution of the partial differential
 equations describing fluid flow.
 .
 This package contains the headers and development libraries needed to
 build applications using libgfs.
 .
 This package has MPI support built in.
