#!/bin/sh

wget -c "http://gerris.dalembert.upmc.fr/gerris/gerris-snapshot.tar.gz"
tar zxvf gerris-snapshot.tar.gz

DFSG='1'
SNAPSHOT=`echo gerris-snapshot-?????? | sed 's/gerris-snapshot-//'`
NEWDIR="gerris-0.9.2+darcs$SNAPSHOT-dfsg.$DFSG"
NEWNAME=`echo $NEWDIR | sed s/-/_/`

rm -rf "gerris-snapshot-$SNAPSHOT/modules/"
mv "gerris-snapshot-$SNAPSHOT" "$NEWDIR"

updatefile() {
	FILE="$1"
	SED="$2"
	TOUCH=`mktemp`
	cp -p "$NEWDIR/$FILE" "$TOUCH"
	sed $SED "$TOUCH" > "$NEWDIR/$FILE"
	touch -r "$TOUCH" "$NEWDIR/$FILE"
	rm "$TOUCH"
}

updatefile 'configure' 's/modules.*test/test/'
updatefile 'configure.in' 's/modules.*Makefile$//'
updatefile 'Makefile.am' 's/modules//'
updatefile 'Makefile.in' 's/modules//'

# tar+gzip

touch -r "gerris-snapshot.tar.gz" "$NEWDIR"
tar -cf "$NEWNAME.orig.tar" "$NEWDIR"
gzip -9fn "$NEWNAME.orig.tar"
rm -fr "$NEWDIR" "gerris-snapshot.tar.gz"
